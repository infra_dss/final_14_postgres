# final_14_postgres

```bash
# ansible-galaxy collection install -r requirements_collestions.yml -p collections

# ansible-galaxy install -r requirements.yml --roles-path roles

# ansible-galaxy install --roles-path .  geerlingguy.haproxy

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

python3 get-pip.py --user

python3 -m pip install --user ansible-core

ansible-galaxy collection install сommunity.general
ansible-galaxy collection install ansible.posix
ansible-galaxy collection install ansible.windows
ansible-galaxy collection install сommunity.postgresql

# python3 -m pip install --user ansible-core==2.12.3

git clone https://github.com/vitabaks/postgresql_cluster.git

cd postgresql_cluster

nano inventory

cp ansible.cfg /etc/ansible/ansible.cfg

# change https to http postgres repo
postgresql_cluster\roles\pgbackrest\tasks\main.yml
postgresql_cluster\vars\Debian.yml

ansible -i inventory all -m ping

ansible-playbook -i inventory deploy_pgcluster.yml

# update run change inventory
[master]
167.71.1.214 hostname=pgsql-master postgresql_exists=true

[replica]
167.71.5.119 hostname=pgsql-replica-1 postgresql_exists=true
134.209.194.194 hostname=pgsql-replica-2 postgresql_exists=true
167.71.5.159 hostname=pgsql-replica-3 postgresql_exists=true

# vars
patroni_ttl: 180

ansible-playbook -i inventory update_pgcluster.yml -e target=patroni


ansible-galaxy collection install prometheus.prometheus
ansible-galaxy collection install grafana.grafana
```
