# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.10.1"
  constraints = "~> 3.0"
  hashes = [
    "h1:Xlgu+XCoky8JVkxGBURRh0DrMldhdUDxdpakcKynEz4=",
    "zh:0e4914c44984bdfc11140198950cbabe09fada993035f1cdf4a55b3e987ab3cc",
    "zh:1671345b9b0d6f65ed14242e7ff0f1792e6fc2fc4c1df1c1526818882c45b9af",
    "zh:1796f31caf0719b0bd65ad1b8ed8711b3942162e5c50a0bd4653332986b930cc",
    "zh:247c6d4a60f4bc595a11d323ea5c3720eb752c4db380e5a8be67c97c03744a22",
    "zh:479a4a1a332aa44aac706c59244c90d83dcaca08feacec81633c2316d1aba1df",
    "zh:53927fbf617a6234db12d74b0f970a65cca004ea13def2138b43286965d40e43",
    "zh:73d0277597b7ccec261538ee49b1ddb1118fe28cf626df43b5e09abbffe6aa5d",
    "zh:7431e3f2d0c254cc3f77bc62ad1eefb8ae7ee52d0de7b28ac90b6a98c67694ea",
    "zh:98d859f6e510ddbe819ffb1c1d6280876298645dd0f74d25f6ed320dc4772b95",
    "zh:c219569db8de5f7b5a402182599a32207dec0fef35c950c7594ceee3e95c4c94",
    "zh:c312da4780bf776e3ef18c9e719b0a2dd0656f047a7ef5058dda3a3a931bb8d8",
    "zh:c752fa6860b3ccbec0ee359cfad805d6c6205366c5889341dc6ee6cb6bef0bdd",
    "zh:cd9f3d4f480a262d3e4eb663fcdfbc788ea6d8063beca19eed26e962c2df3725",
    "zh:f569573661f68e72e9044b8cf76ee40f652b19c788c0e26baf52bfbf84c9f26f",
  ]
}

provider "registry.terraform.io/sergsoloviev/vscale" {
  version     = "0.0.3"
  constraints = "0.0.3"
  hashes = [
    "h1:1/er4KAopuQCwHDTEcc+ibL5rIYB0IXJmo0wHqiT1Yc=",
    "zh:0e1db3a128557a277dc2db825d14769176bbc1f56081954244b1142b31e8cf9a",
    "zh:232c05124288ab40047127fa8877be0e656701dfce13923ee7d2b19eb83a8afb",
    "zh:362b20e87b2a709d48e95212154d3d74ba70ce78e4aeef8dbe385a52d6e1642d",
    "zh:4e7eedce52dcae3e2d888a2e43e22f15de44c54ff648a95918b852ff65ef23f2",
    "zh:770f9fbc2729de60671ee9d83c46857079ce03854c15ec11fc0842a4ab347538",
    "zh:7a44aa92056b50a0cc39900aaa915042e0320034169fd9d2c8896089e02cde12",
    "zh:7bef0c6e11ffda1948985a17cdec0835b0608ce28ae7d1d9acb2c9ed43b39af9",
    "zh:83405cb41e7b0e52e7621a8a485ad6bf5a73c07fe8964d246cc51538d0fe8d91",
    "zh:9f0fb7f0e8ca10c17e14ab9332fd2bf73ea2eff94c934d698edcf249c8a8752f",
    "zh:c643fc432e6c59f10826d30d2dd5ecde44633e5d2c79f2e354757940ba44ea57",
    "zh:ef18de4c7578948c79172ca9bb31a665d46c139e47a9df5dce67f55ce79f2e7a",
    "zh:f9f966b4d1aebaa3750bdcdd8cc61d54b22dde1c9c26f0be4d3bb5d45a74bf70",
  ]
}
