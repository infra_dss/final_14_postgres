resource "cloudflare_record" "haproxy" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["haproxy-reb"] )
  name     = each.key
  type    = "A"
  value   = "167.71.69.5"
  proxied = false
}

resource "cloudflare_record" "etcd_1" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["etcd-1-reb"] )
  name     = each.key
  type    = "A"
  value   = "134.209.81.58"
  proxied = false
}

resource "cloudflare_record" "etcd_2" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["etcd-2-reb"] )
  name     = each.key
  type    = "A"
  value   = "134.209.81.58"
  proxied = false
}

resource "cloudflare_record" "etcd_3" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["etcd-3-reb"] )
  name     = each.key
  type    = "A"
  value   = "134.209.81.58"
  proxied = false
}

resource "cloudflare_record" "pgbouncer_1" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgbouncer-1-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}

resource "cloudflare_record" "pgbouncer_2" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgbouncer-2-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}

resource "cloudflare_record" "pgsql_master" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgsql-master-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}

resource "cloudflare_record" "pgsql_replica_1" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgsql-replica-1-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}

resource "cloudflare_record" "pgsql_replica_2" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgsql-replica-2-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}

resource "cloudflare_record" "pgsql_replica_3" {
  zone_id = var.cloudflare_zone_id
  for_each = toset( ["pgsql-replica-3-reb"] )
  name     = each.key
  type    = "A"
  value   = "89.208.104.45"
  proxied = false
}