terraform {
  required_version = ">= 0.15.0"
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    vscale = {
      source  = "sergsoloviev/vscale"
      version = "0.0.3"
    }
  }
}

provider "vscale" {}

provider "cloudflare" {
  email = var.email
  api_key = var.token
}
