variable "location" {
  description = "location"
  type        = string
  default     = "msk0"
}

variable "instance_type" {
  description = "Enter Instance Type"
  type        = string
  default     = "small"
}

variable "key_name" {
  description = "SSH key for server"
  type        = list
  default     = ["wsl"]
}

variable "instance_image" {
  description = "Enter Instance image"
  type        = string
  default     = "ubuntu_20.04_64_001_master"
}

variable "srv_vpn_name" {
  description = "srv_name"
  type        = string
  default     = "vpn"
}

variable "domain" {
  description = "dns zone"
  type        = string
  default     = "littleblog.ru"
}


variable "cloudflare_zone_id" {
  description = "dns zone"
  type        = string
  default     = "60130eb28335c3947e0ccabca66cfe56"
}


variable "email" {}
variable "token" {}
